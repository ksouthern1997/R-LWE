import math
import copy

def cpoly(n):
    if(n==1):
        return[-1,1]
    elif prime(n):
        poly=[]
        for i in range(0,n):
            poly.append(1)
        return poly
    elif (n/2)%1==0 and (n/2)%2==1:
        poly=cpoly(int(n/2))
        for i in range(1,len(poly)):
            if i%2==1:
                poly[i]=-poly[i]
        return poly
    elif math.log(n,2)%1==0:
        poly=[]
        poly.append(1)
        for i in range(1, int(n/2)):
            poly.append(0)
        poly.append(1)
        return poly
    elif primePower(n)[0]:
        p=0
        m=0
        x=0
        [x,p,m]=primePower(n)
        prePoly=cpoly(p)
        poly=[0]*len(prePoly)*(p**(m-1))
        poly[0]=1
        for i in range(1,len(prePoly)):
            if prePoly[i]!=0:
                poly[i*(p**(m-1))]=prePoly[i]
        while(poly[-1]==0):
            poly=poly[0:-1]
        return poly
    elif primePowerFactor(n)[0]:
        p=0
        m=0
        r=0
        x=0
        [x,p,m,r]=primePowerFactor(n)
        prePoly=cpoly(p*r)
        poly=[0]*len(prePoly)*(p**(m-1))
        poly[0]=1
        for i in range(1,len(prePoly)):
            if prePoly[i]!=0:
                poly[i*(p**(m-1))]=prePoly[i]
        while(poly[-1]==0):
            poly=poly[0:-1]
        return poly
    else:
        [n1,p]=primeAndOther(n)
        prePoly=cpoly(n1)
        numerator=[0]*len(prePoly)*p
        for i in range(0,len(prePoly)):
            if prePoly[i]!=0:
                numerator[i*p]=prePoly[i]
        d=cpoly(n1)
        while(numerator[-1]==0):
            numerator=numerator[0:-1]
        return div(numerator,d)
    return("Nope")
        
def getPrimeFactors(n):
    if n==1:
        return[]
    for i in range(2,n+1):
        if(n%i==0):
            return [i]+getPrimeFactors(int(n/i))

def prime(p):
    n=getPrimeFactors(p)
    if n==[p]:
        return True
    return False

def primePower(n):
    num=getPrimeFactors(n)
    if num==[num[0]]*(len(num)):
        return [True,num[0],len(num)]
    return[False,0,0]

def primePowerFactor(n):
    num=getPrimeFactors(n)
    num=count(num)
    contenders=[]
    for item in num:
        if item[1]>1:
            contenders.append([item[0],item[1]-1])
    if len(contenders)==0:
        return [False,0,0,0]
    i=0
    highest=0
    c=0
    for c in range(0,len(contenders)):
        if contenders[c][0]**contenders[c][1]>highest:
            highest=contenders[c][0]**contenders[c][1]
            i=c
    c=0
    while num[c][0]!=contenders[i][0]:
        c=c+1
    return [True,num[c][0],num[c][1],int(n/(num[c][0]**num[c][1]))]

def primeAndOther(n):
    num=getPrimeFactors(n)
    p=num[-1]
    n=int(n/p)
    return [n,p]
        
def count(n):
    num=[]
    num.append([n[0],1])
    for item in n[1:]:
        if num[-1][0]==item:
            num[-1][1]=num[-1][1]+1
        else:
            num.append([item,1])
    return num

def div(n,d):
    workN=n
    workD=d
    q=[]
    while len(workN)>=len(workD):
        q1=int(workN[0]/workD[0])
        q.append(q1)
        for i in range(0,len(workD)):
            workD[i]=workD[i]*q1
        for i in range(0,len(workD)):
            workN[i]=workN[i]-workD[i]
        workD=copy.deepcopy(d)
        workN=workN[1:]
    return q
for i in range(1,31):
    print(str(i)+":"+str(cpoly(i)))
