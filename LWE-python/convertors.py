def fullBinToHex(binCode):
    hexCode=""
    while(binCode!=""):
        hexCode=hexCode+(binToHex(binCode[0:4]))
        binCode=binCode[4:]
    return hexCode
#hex to binary convertor
def hexToBin(a):
    binary=""
    for char in a:
        binary=binary+hexToBin1(char)
    return binary
def hexToBin1(a):
    bitstring=""
    a=hexToInt(a)
    a=int(a)
    if a>=8:
        a=a-8
        bitstring=bitstring+"1"
    else:
        bitstring=bitstring+"0"
    if a>=4:
        a=a-4
        bitstring=bitstring+"1"
    else:
        bitstring=bitstring+"0"
    if a>=2:
        a=a-2
        bitstring=bitstring+"1"
    else:
        bitstring=bitstring+"0"
    if a>=1:
        a=a-1
        bitstring=bitstring+"1"
    else:
        bitstring=bitstring+"0"
    return bitstring

#hex to int
def hexToInt(a):
    if a=="a" or a=="A":
        a=10
    elif a=="b" or a=="B":
        a=11
    elif a=="c" or a=="C":
        a=12
    elif a=="d" or a=="D":
        a=13
    elif a=="e" or a=="E":
        a=14
    elif a=="f" or a=="F":
        a=15
    return int(a)

def binToHex(a):
    a=binToInt(a)
    if a==10:
        a="A"
    elif a==11:
        a="B"
    elif a==12:
        a="C"
    elif a==13:
        a="D"
    elif a==14:
        a="E"
    elif a==15:
        a="F"
    else:
        a=str(a)
    return a

#Takes as input a binary string, returns an int
def binToInt(state):
    i=0
    outInt=0
    while(len(state)>0):
        outInt=outInt+(int(state[-1])*(2**i))
        i=i+1
        state=state[0:-1]
    return outInt

#Takes as input two ints, number and bits
#outputs the binary representation of number of length "bits"
def intToBin(state, bits):
    state=int(state)
    bitstring=""
    for x in range(0,bits):
        y=2**(bits-x-1)
        if state>=y:
            state=state-y
            bitstring=bitstring+"1"
        else:
            bitstring=bitstring+"0"
    return(bitstring)
