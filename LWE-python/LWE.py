import math
import numpy as np
import random
import convertors
def keyGenerator(m,q):
    #check validity (n^2 < q < 2n^2, m=5n, q is prime)
    n=int(m/5)
    if q<n**2 or q>2*n**2:
        print("Error: Invalid m,q")
        return("Error: Invalid m,q")
    else:
        nprime=math.ceil(math.sqrt(q))
        for i in range(2,nprime+1):
            if(q%i)==0:
                print("Error: q is not prime")
                return ("Error: q is not prime")
    #generate s
    s=[]
    for i in range(0,n):
        s.append(random.randint(0,q-1))
    s=np.matrix(s)
    s=np.transpose(s)
    #generate a_0 to a_m
    a=[]
    for i in range(0,m):
        ap=[]
        for i in range(0,n):
            ap.append(random.randint(0,q-1))
        a.append(ap)
    a=np.matrix(a)
    #bprime=s.a
    bprime=np.matmul(a,s)
    #b=b+e
    e=[]
    psi=q/(n*math.log(n,2))
    for i in range(0,m):
        e.append(round(np.random.normal(0,psi)))
    e=np.matrix(e)
    e=np.transpose(e)
    b=np.add(bprime,e)%q
    #b=bprime
    private=s
    public=[a,b]
    return([private,public])
    #private=[s]
    #public =[a,b]
        
def encrypt(bit,public,q):
    #pick set of ints from [0,m] nArray=[] for i in range(0,m): if random.rand()>0.5:
    aprime=[]
    bprime=[]
    #for i in range(0,len(public[0])):
    #    if random.random()<0.2:
    #        aprime.append(public[0][i])
    #        bprime.append(public[1][i])
    for i in range(2,10):
        aprime.append(public[0][i])
        bprime.append(public[1][i])


    #sum ai's 
    #a=0 for item in nArray: a=a+public[0][item] #N.B a is a vector
    a=aprime[0]
    for i in range(1,len(aprime)):
        a=np.add(a,aprime[i])%q
         
    #sum bi's + bit/2 mod q
    #b=0 for item in nArray: b=b+public[1][item] b=b%q
    b=0
    for i in bprime:
        b=b+i
    b=(b+math.floor(bit*(q/2)))%q
    return [a,b]
    #return a,b

def decrypt(pair,private,q):
    [a,b]=pair
    #N.B. [a,b] is the output from encrypt 
    #calc b- a*s/q
    s=private
    z=np.matmul(a,s)
    z=z%q
    r=(b-z)%q
    border=math.floor(q/2)/2
    if r<=border or r>=q-border:
        return 0
    else:
        return 1
    
    #need to use numpy to find a*s, should be most efficient way, good place for improving
    #np.matmult(a,b)
    #calc bit value
    #return bit
m=110
q=691
[private,public]=keyGenerator(m,q)


message="Some random message that will be encrypted. "
chars=[]
for letter in message:
    chars.append(ord(letter))
binary=""
for letter in chars:
    binary=binary+convertors.intToBin(letter,8)
toSend=[]
for bit in binary:
    toSend.append(encrypt(int(bit),public,q))
#print(toSend)
rec=""
for pair in toSend:
    rec=rec+str(decrypt(pair,private,q))

nums=[]
for i in range(0,int(len(rec)/8)):
    nums.append(convertors.binToInt(rec[int(i*8):int((i+1)*8)]))
messageRec=""
for num in nums:
    messageRec=messageRec+chr(int(num))
print("orginal:   "+message)
print("decrypted: "+messageRec)
    
