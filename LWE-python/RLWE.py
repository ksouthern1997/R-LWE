'''
Based off the paper by LPR https://www.iacr.org/archive/eurocrypt2010/66320288/66320288.pdf
'''
import numpy as np
import math
import copy
import matplotlib
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt

np.set_printoptions(threshold=np.inf)
from numpy.polynomial import polynomial as p

# n=4096

sd = 0
s = 0


def Rq():
    return np.floor(np.random.random(size=n) * q) % q


def R2():
    return np.floor(np.random.random(size=n) * q) % 2


def ring_mult(r1, r2):
    r3 = np.polymul(r1, r2)
    correctLen = False
    if len(r3) < n:
        m = n - len(r3)
        r3 = np.concatenate((np.zeros(m), r3))
        correctLen = True

    while not correctLen:
        if len(r3) == n:
            break
        if len(r3) < n:
            print("What?")
        r3 = np.floor(p.polydiv(r3, fx)[1] % q)
    if len(r3) < n:
        print("How?")
        m = n - len(r3)
        r3 = np.concatenate((np.zeros(m), r3))
    return r3


def keygen():
    A = []
    R = []
    for i in range(0, m):
        A.append(Rq())
        R.append(R2())
    a_m = 0
    for i in range(0, m):
        a_m = np.add(a_m, ring_mult(R[i], A[i])) % q
    A.append(a_m)
    R.append(-1)
    return [A, R]  # A=public, R=private.


def encrypt(public, z):  # z is a bitstring of length n (the message)
    global s
    s = Rq()
    B = []
    for i in range(0, m + 1):
        B.append(np.add(ring_mult(public[i], s), error_dist(n, q)) % q)  # b=a.s+e
    B[-1] = np.ceil(np.subtract(B[-1], ring_mult(z, q / 2))) % q
    return B


def decrypt(private, cipher):  # private is R, cipher is B
    a = 0
    for i in range(0, len(cipher)):
        a = np.add(a, ring_mult(private[i], cipher[i])) % q
    #a = np.add(a, error_dist(n, q))
    message = []
    for bit in a:
        if (bit < (q / 4)) or (bit > (3 * q / 4)):
            message.append(0)
        else:
            message.append(1)
    return message


def error_dist(n, q):  # size n, mod q
    global sd
    global fx
    correctLen = False
    if sd == 0:
        var = 1
    else:
        var = sd
    while not correctLen:
        poly = np.floor(np.random.normal(0, var, size=n))  # q**0.75
        poly = np.floor(p.polydiv(poly, fx)[1] % q)
        if len(poly) == n:
            correctLen = True
    if sd == 0:
        poly = np.zeros(n)
    return poly


def inverse(num):
    inv=copy.deepcopy(num)
    for i in range(0,q-3):
        inv = ring_mult(inv,num)%q
    return inv






n = 2048
#n=3
fx = [1] + [0] * (n - 1) + [1]
q=105943
#q=7

#print(ring_mult([2,3,4],inverse([2,3,4])))


y = []
x1 = []
x2 = []
for sd in range(0, 128):
    m=math.floor(math.log(q,2))
    #m = 20
    [public, private] = keygen()
    pt = R2()
    # print("Plain Text")
    # print(pt)
    ct = encrypt(public, pt)
    ct2 = copy.deepcopy(ct)
    # print("Cipher Text")
    # print(ct[-1])
    pt2 = decrypt(private, ct)
    test = ct2[-1]
    for i in range(0, m):
        test += ring_mult(ct2[i], (public[i])) % q
    test = np.round(test / m)
    breakAttempt = ct2[-1]
    breakAttempt = np.subtract(breakAttempt, ring_mult(public[-1], test))
    result = []
    for i in breakAttempt:
        z = 0
        if i < math.floor(q / 4) or 1 > 3 * math.floor(q / 4):
            z = 0
        else:
            z = 1
        result.append(z)
    breakAttempt = []

    # print("Decoded text")
    # print(pt2)
    # if np.allclose(pt,pt2):
    #    print("Correct")
    # else:
    #    print("WRONG")
    right = 0
    guessed = 0
    random = 0
    # one=0
    # calc=0
    for i in range(0, len(pt)):
        if (pt[i] == pt2[i]):
            right += 1
        if (pt[i] == result[i]):
            guessed += 1
        # if(pt[i]==1):
        #    one+=1
    print(sd)
    print(str(100 * right / n) + "% correct")
    # print(str(100*random/n)+"% that could be guessed from CT[naively]")
    y.append(sd)
    x1.append(100 * right / n)
    x2.append(100 * guessed / n)
    print(str(100 * guessed / n) + "% that could be guessed from CT[subtracted the array]")
    # print(str(100*one/n)+"% guessd correctly by assuming all 1's")
    # print(str(100*fuckedup/n)+"% that were out of range?")

plt.xlabel("error, NB p= " + str(q))
plt.ylabel("decode %")
plt.plot(y, x1, marker='.', color='b', linestyle='None')
plt.plot(y, x2, marker='.', color='r', linestyle='None')
plt.show()

